package com.homeplus.dtos.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserPutDto {
    private Long id;

    private String email;

    private String name;

    private String password;

    private String gender;

    private String language;

    private String state;

    private String street;

    private String postcode;

    private Integer mobile;

    private Boolean is_tasker;

    private Boolean is_tasker_data;

    private String status;

    private String token;

    private OffsetDateTime createdTime;

    private final OffsetDateTime updated_time = OffsetDateTime.now();
}
