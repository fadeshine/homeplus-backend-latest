package com.homeplus.services;

import com.homeplus.HomeplusApplication;
import com.homeplus.dtos.task.TaskGetDto;
import com.homeplus.googleMapsApi.GeocodeApi;
import com.homeplus.models.*;
import com.homeplus.repositories.TaskRepository;
import com.homeplus.repositories.TaskerRepository;
import com.homeplus.utility.mapper.TaskMapper;
import com.homeplus.utils.Utility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = HomeplusApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TaskServiceTest {
    @Mock
    private TaskRepository taskRepository;

    @Mock
    private TaskerRepository taskerRepository;

    @Autowired
    private TaskMapper taskMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private TaskerService taskerService;

    @Autowired
    private TimeSectionsService timeSectionsService;

    @Autowired
    private GeocodeApi geocodeApi;

    @Autowired
    private Utility utility;

    TaskService taskService;


    @BeforeEach
    void setup() {
        taskService = new TaskService(taskRepository, taskMapper, userService, taskerService, timeSectionsService, geocodeApi);
    }

    int min = 0;
    int max = 100000;
    Integer random_int1 = (int)Math.floor(Math.random()*(max-min+1)+min);
    Integer random_int2 = (int)Math.floor(Math.random()*(max-min+1)+min);
    Integer random_int6 = (int)Math.floor(Math.random()*(max-min+1)+min);

    @Test
    public void shouldReturnAllTasks() {
        UserEntity userEntity1 = utility.buildUserEntity("ting" + random_int1 + "@gmail.com","Davin1", "testingOKOK1","M","En","placeholder","placeholder","placeholder",new Date(),04211252, true, "p", "p1", OffsetDateTime.now(),OffsetDateTime.now());
        TaskerEntity taskerEntity1 = utility.buildTaskerEntity(userEntity1, "q1","q","IT support", "testing", "063-963", "132621233",OffsetDateTime.now(),OffsetDateTime.now());
        TimeSectionsEntity timeSectionsEntity1 = utility.buildTimeSectionsEntity("pnn", "vd");

        TaskEntity task1 = utility.buildTaskEntity(userEntity1, taskerEntity1, "Clean my house", "testing", OffsetDateTime.now(), timeSectionsEntity1, 5526L, "d","d","d",3022L,"d",7,7,9,true,true,"d","d","d","d",1, TaskStatus.open, OffsetDateTime.now(), OffsetDateTime.now());

        List<TaskGetDto> tasks = taskService.getAllTasks();
        assertNotNull(userEntity1);
        assertNotNull(taskerEntity1);
        assertNotNull(timeSectionsEntity1);
        assertNotNull(task1);
        assertNotNull(tasks);
    }


    @Test
    public void shouldReturnTasksGivenKeyword() {
        UserEntity userEntity1 = utility.buildUserEntity("ting" + random_int1 + "@gmail.com","Davin1", "testingOKOK1","M","En","placeholder","placeholder","placeholder",new Date(),04211252, true, "p", "p1", OffsetDateTime.now(),OffsetDateTime.now());
        TaskerEntity taskerEntity1 = utility.buildTaskerEntity(userEntity1, "q1","q","IT support", "testing", "063-963", "132621233",OffsetDateTime.now(),OffsetDateTime.now());
        TimeSectionsEntity timeSectionsEntity1 = utility.buildTimeSectionsEntity("pnn", "vd");

        TaskEntity task1 = utility.buildTaskEntity(userEntity1, taskerEntity1, "Clean my house", "testing", OffsetDateTime.now(), timeSectionsEntity1,5526L, "d","d","d",3022L,"d",7,7,9,true,true,"d","d","d","d",1, TaskStatus.open, OffsetDateTime.now(), OffsetDateTime.now());

        List<TaskGetDto> tasksByUserIdAndKeyword = taskService.getTasksByUidAndKeyword(1L, "Clean");
        List<TaskGetDto> tasksByTaskerIdAndKeyword = taskService.getTasksByTidAndKeyword(1L, "Clean");
        assertNotNull(userEntity1);
        assertNotNull(taskerEntity1);
        assertNotNull(timeSectionsEntity1);
        assertNotNull(task1);
        assertNotNull(tasksByUserIdAndKeyword);
        assertNotNull(tasksByTaskerIdAndKeyword);
    }

    @Test
    public void shouldReturnUpComingTasks() {
        UserEntity userEntity1 = utility.buildUserEntity("ting" + random_int1 + "@gmail.com","Davin1", "testingOKOK1","M","En","placeholder","placeholder","placeholder",new Date(),04211252, true, "p", "p1", OffsetDateTime.now(),OffsetDateTime.now());
        TaskerEntity taskerEntity1 = utility.buildTaskerEntity(userEntity1, "q1","q","IT support", "testing", "063-963", "132621233",OffsetDateTime.now(),OffsetDateTime.now());
        TimeSectionsEntity timeSectionsEntity1 = utility.buildTimeSectionsEntity("pnn", "vd");

        TaskEntity task1 = utility.buildTaskEntity(userEntity1, taskerEntity1, "Clean my house", "testing", OffsetDateTime.now(), timeSectionsEntity1, 5526L, "d","d","d",3022L,"d",7,7,9,true,true,"d","d","d","d",1, TaskStatus.open, OffsetDateTime.now(), OffsetDateTime.now());

        List<TaskGetDto> tasksByUid = taskService.getUpComingTasksByUid(1L);
        List<TaskGetDto> tasksByTid = taskService.getUpComingTasksByTid(1L);
        assertNotNull(userEntity1);
        assertNotNull(taskerEntity1);
        assertNotNull(timeSectionsEntity1);
        assertNotNull(task1);
        assertNotNull(tasksByUid);
        assertNotNull(tasksByTid);
    }

}




